<?php

namespace DeliveryDotCom\Models;

use DeliveryDotCom\Contracts\OrderInterface;
use DeliveryDotCom\Contracts\ItemInterface;
use DeliveryDotCom\Contracts\PaymentInterface;

/**
 * Class for an individual order
 *
 * @method void addItem()
 * @method void addPayment()
 * @method bool isPaidInFull()
 */
class Order implements OrderInterface
{
    private $_items = array();
    private $_payments = array();

    /**
     * Adds item to array of items in the order
     *
     * @param ItemInterface $item An item that is part of the order
     *
     * @return void
     */
    public function addItem(ItemInterface $item)
    {
        try {
            $this->_items[] = $item;
        } catch (Exception $e) {
            die("Could not add $item to items array\n");
        }
    }

    /**
     * Adds payment to array of payments for the order
     *
     * @param PaymentInterface $payment A payment for the order
     *
     * @return void
     */
    public function addPayment(PaymentInterface $payment)
    {
        try {
            $this->_payments[] = $payment;
        } catch (Exception $e) {
            die("Could not add $payment to payments array\n");
        }
    }

    /**
     * Returns total cost of all items
     *
     * @return int $totalOwed Total cost of all items
     */
    public function getTotalOwed()
    {
        if (empty($this->_items)) {
            die("No items added");
        }

        $totalOwed = 0;
        foreach ($this->_items as $item) {
            $totalOwed += $item->getAmount();
        }
        return $totalOwed;
    }

    /**
     * Returns total amount paid
     *
     * @return int $totalPaid Total amount paid via payments
     */
    public function getTotalPaid()
    {
        if (empty($this->_payments)) {
            die("No payments added");
        }

        $totalPaid = 0;
        foreach ($this->_payments as $payment) {
            $totalPaid += $payment->getAmount();
        }
        return $totalPaid;
    }

    /**
     * Determines if order has been paid in full
     *
     * @return boolean true if the order has been paid in full, false if not.
     */
    public function isPaidInFull()
    {
        return ($this->getTotalOwed() <= $this->getTotalPaid() ? true : false);
    }
}
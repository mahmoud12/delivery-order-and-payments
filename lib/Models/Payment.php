<?php

namespace DeliveryDotCom\Models;

use DeliveryDotCom\Contracts\PaymentInterface;

/**
 * Class for an individual payment
 *
 * @method int getAmount()
 */
class Payment implements PaymentInterface
{
    private $_amount;

    /**
     * Constructor for Payment class
     *
     * @param int $amount Amount paid for the order
     */
    function __construct($amount)
    {
        if (!\is_int($amount)) {
            throw new \Exception('Parameter is not an integer');
        }
        $this->_amount = $amount;
    }
    /**
     * Returns amount if individual payment
     *
     * @return int The amount of an individual payment
     */
    public function getAmount()
    {
        return $this->_amount;
    }
}
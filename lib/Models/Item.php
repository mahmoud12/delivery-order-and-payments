<?php

namespace DeliveryDotCom\Models;

use DeliveryDotCom\Contracts\ItemInterface;

/**
 * Class for an individual item
 *
 * @method int getAmount()
 */
class Item implements ItemInterface
{
    private $_amount;

    /**
     * Constructor for Item class
     *
     * @param int $amount Price for the item
     */
    function __construct($amount)
    {
        if (!\is_int($amount)) {
            throw new \Exception('Parameter is not an integer');
        }
        $this->_amount = $amount;
    }

    /**
     * Returns the price of an individual item
     *
     * @return int The price of the individual item
     */
    public function getAmount()
    {
        return $this->_amount;
    }
}
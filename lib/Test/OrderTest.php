<?php

namespace DeliveryDotCom\Test;

use \PHPUnit\Framework\TestCase;
use DeliveryDotCom\Contracts\OrderInterface;
use DeliveryDotCom\Contracts\ItemInterface;
use DeliveryDotCom\Contracts\PaymentInterface;
use DeliveryDotCom\Models\Order;
use DeliveryDotCom\Models\Payment;
use DeliveryDotCom\Models\Item;


class OrderTest extends TestCase
{
    /**
     * Test if paid in full
     *
     * @dataProvider providerPaidInFull
     *
     * @param array $owed Test for amount owed
     * @param array $paid Test for amount paid
     *
     * @return void
     */
    function testIsPaidInFull($owed, $paid)
    {
        $order = new Order();
        foreach ($owed as $owe) {
            $order->addItem(new Item($owe));
        }
        foreach ($paid as $pay) {
            $order->addPayment(new Payment($pay));
        }
        $this->assertEquals(
            $order->isPaidInFull(),
            true
        );
    }

    /**
     * Provides test cases for PaidInFull
     *
     * @return array Array of test cases
     */
    public function providerPaidInFull()
    {
        return array(
            array(array(10, 20, 40), array(10, 20, 40)),
            array(array(10, 20, 40), array(3, 7, 15, 15, 30)),
            array(array(10, 20, 40), array(5, 25, 40))
        );
    }
}
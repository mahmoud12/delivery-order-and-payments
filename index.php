<?php

require __DIR__ . '/vendor/autoload.php';


use DeliveryDotCom\Models\Item;
use DeliveryDotCom\Models\Order;
use DeliveryDotCom\Models\Payment;


/*
Order is paid in full
*/
$order = new Order();
$order->addItem(new Item(25));
$order->addItem(new Item(8));
$order->addPayment(new Payment(15));
$order->addPayment(new Payment(18));

echo json_encode($order->isPaidInFull()) . "\n";


/*
Order is NOT paid in full
Payment is too low
*/
$order = new Order();
$order->addItem(new Item(25));
$order->addItem(new Item(8));
$order->addItem(new Item(6));
$order->addPayment(new Payment(25));

echo json_encode($order->isPaidInFull()) . "\n";


/*
Order is overpaid
Payment is higher
*/
$order = new Order();
$order->addItem(new Item(8));
$order->addItem(new Item(8));
$order->addItem(new Item(6));
$order->addPayment(new Payment(50));

echo json_encode($order->isPaidInFull()) . "\n";